/*
 * @Author: your name
 * @Date: 2021-08-21 11:25:00
 * @LastEditTime: 2021-08-21 11:37:23
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \系统学习\VueProject\vue02\supermall\postcss.config.js
 */
module.exports = {
    Plugin:{
        autoprefixer:{},
        "postcss-px-to-viewport":{
            viewportWidth:375,//视窗的宽度，对应的是我们设计稿的宽度
            viewportHeight:667,//视窗的高度，对应的是我们设计稿的高度 (也可以不配置）
            unitPrecision:5,//制定‘px’转换为视窗单位的小数位数（很多时候无法整除）
            viewportUnit:'vw',//指定需要转换成的视窗单位，建议使用vw
            selectorBlackList:['ignore'],//指定不需要转换的类，这里是正则的规则
            minPixelValue:1,//小于或等于‘1px’不转换为视窗单位
            mediaQuery:false//允许在媒体查询中转换为‘px’
        },
    }
}
