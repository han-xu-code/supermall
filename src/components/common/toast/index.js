/*
 * @Author: your name
 * @Date: 2021-08-21 09:45:22
 * @LastEditTime: 2021-08-21 10:10:10
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \系统学习\VueProject\vue02\supermall\src\components\common\toast\index.js
 */
import Toast from './Toast'
const obj = {}
obj.install = function (Vue) {
    // console.log('执行',Vue);
    // console.log(Toast.$el); // undefined
    // document.body.appendChild(Toast.$el)

    // 1. 插件组件构造器
    const toastConstructor = Vue.extend(Toast)
    
    // 2. new的方式，根据组件构造器，可以创建一个组件对象
    const toast = new toastConstructor()

    // 3. 将组件对象手动挂载到某一个元素上
    toast.$mount(document.createElement('div'))

    // 4. toast.$el对应的就是div
    document.body.appendChild(toast.$el)


    
    Vue.prototype.$toast = toast
}
export default obj