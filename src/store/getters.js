/*
 * @Author: your name
 * @Date: 2021-08-19 22:21:23
 * @LastEditTime: 2021-08-19 23:19:46
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \系统学习\VueProject\vue02\supermall\src\store\getters.js
 */
export default {
    cartLength(state) {
        return state.cartList.length
    },
    cartList(state){
        return state.cartList
    }
}