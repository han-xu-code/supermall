/*
 * @Author: your name
 * @Date: 2021-08-19 18:42:50
 * @LastEditTime: 2021-08-20 21:21:30
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \系统学习\VueProject\vue02\supermall\src\store\actions.js
 */
import { ADD_COUNTER, ADD_TO_CART } from "./mutation-types"
export default {
    addCart(context, payload) {
        return new Promise((resolve, reject) => {
            let oldProduct = context.state.cartList.find(item => item.iid === payload.iid)
            if (oldProduct) {
                context.commit(ADD_COUNTER, oldProduct)
                resolve('当前的商品数量+1')
            } else {
                payload.count = 1
                context.commit(ADD_TO_CART, payload)
                resolve('添加了新的商品')
            }
        })
    }
}