/*
 * @Author: your name
 * @Date: 2021-08-19 18:40:53
 * @LastEditTime: 2021-08-20 20:00:10
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \系统学习\VueProject\vue02\supermall\src\store\mutations.js
 */
import { ADD_COUNTER,ADD_TO_CART } from "./mutation-types"
export default {
    [ADD_COUNTER](state,payload){
        payload.count++
    },
    [ADD_TO_CART](state,payload){
        payload.checked = true
        state.cartList.push(payload)
    }
}
