/*
 * @Author: your name
 * @Date: 2021-08-19 16:57:18
 * @LastEditTime: 2021-08-19 22:21:58
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \系统学习\VueProject\vue02\supermall\src\store\index.js
 */
import Vue from 'vue'
import Vuex from 'vuex'
import mutations from './mutations'
import actions from './actions'
import getters from './getters'
// 1. 安装插件
Vue.use(Vuex)

// 2. 创建Store对象
const state = {
    cartList:[]
}

const store = new Vuex.Store({
    state,
    mutations,
    actions,
    getters
})
// 3. 挂载到Vue实例上
export default store