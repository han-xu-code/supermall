/*
 * @Author: your name
 * @Date: 2021-08-19 16:57:18
 * @LastEditTime: 2021-08-19 18:55:16
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \系统学习\VueProject\vue02\supermall\src\store\index.js
 */
// import Vue from 'vue'
// import Vuex from 'vuex'

// // 1. 安装插件
// Vue.use(Vuex)

// // 2. 创建Store对象
// const store = new Store({
//     state:{
//         cartList:[]
//     },
//     mutations:{
//         // mutations唯一的目的就是修改state中的状态
//         // mutations中的每个方法尽可能完成的事件比较单一一点
//         addCounter(state,payload){
//             payload.count++
//         },
//         addCart(state,payload){
//             state.cartList.push(payload)
//         }
        
//     },
//     actions:{
//         addCart(context,payload){
//             // payLoad新添加的商品
//             // state.cartList.push(payload)
//             // 1. 查找之前数据中是否有该商品
//             let oldProduct = context.state.cartList.find(item => item.iid === payload.iid)
//             // 2. 判断oldProduct
//             if(oldProduct){
//                 context.commit('addCounter',oldProduct)
//             } else{
//                 payload.count = 1
//                 context.commit('addCart',payload)
//             }
//         }
//     }
// })
// // 3. 挂载到Vue实例上
// export default store