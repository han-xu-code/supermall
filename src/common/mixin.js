/*
 * @Author: your name
 * @Date: 2021-08-18 15:20:19
 * @LastEditTime: 2021-08-19 16:22:34
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \系统学习\VueProject\vue02\01supermall\src\common\mixin.js
 */
import {debounce} from 'common/utils'
import BackTop from 'components/content/backTop/BackTop'
export const itemListenerMixin = {
    data(){
        return {
            itemImgListeren:null,
            refresh:null
        }
    },
    mounted(){
        // 图片加载完成的事件监听
        this.refresh = debounce(this.$refs.scroll.refresh,200)
        // 对监听的事件进行保存
        this.itemImgListeren = () => {
            this.refresh()
        }
        // 取消改事件 
        this.$bus.$off('itemImageLoad',this.itemImgListeren)
        // console.log('我是混入中的内容');
    }
}

export const backTopMixin = {
    components:{
        BackTop
    },
    data(){
        return {
            isShowBackTop:false
        }
    },
    methods:{
        backTop(){
            this.$refs.scroll.scrollTop(0,0,300)
        }
    }
}