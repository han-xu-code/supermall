/*
 * @Author: your name
 * @Date: 2021-08-09 16:37:52
 * @LastEditTime: 2021-08-13 08:52:29
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \系统学习\VueProject\vue02\learnaxios\src\network\request.js
 */
import axios from 'axios'

export function request(config) {
    // 1.创建axios实例
    const instance = axios.create({
        baseURL: 'http://152.136.185.210:7878/api/m5',
        timeout: 5000
    })
    // 2.axios的拦截器
    // 拦截请求 (配置信息和请求头)
    // 2.1 请求拦截的作用
    instance.interceptors.request.use(config => {
        return config
    },err => {
        // console.log(err);
    })
    // 拦截响应
    instance.interceptors.response.use(res => {
        // 成功的拦截，返回的是结果
        // console.log(res);
        // 我们要的真正的结果是里面的data
        return res.data
    },err => {
        // 失败的拦截
        console.log(err);
    })
    // 3.发送真正的网络请求  返回的是一个promise
    return instance(config)
}


