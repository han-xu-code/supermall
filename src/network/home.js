/*
 * @Author: your name
 * @Date: 2021-08-13 08:41:52
 * @LastEditTime: 2021-08-13 15:44:03
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \系统学习\VueProject\vue02\supermall\src\network\home.js
 */
import {request} from './request'

export function getHomeMultidata() {
    return request({
        url:'/home/multidata'
    })
}

export function getHomeGoods(type,page) {
    return request({
        url:'/home/data',
        params:{
            type,
            page
        }
    })
}