/*
 * @Author: your name
 * @Date: 2021-08-16 21:22:52
 * @LastEditTime: 2021-08-18 14:11:21
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \系统学习\VueProject\vue02\supermall\src\network\detail.js
 */
import { request } from './request'
export function getDetail(iid) {
    return request({
        url: '/detail',
        params: {
            iid
        }
    })
}
// 推荐页的请求接口
export function getRecommend(){
    return request({
        url:'/recommend'
    })
}
export class Goods{ // 商品详细信息
    constructor(itemInfo, columns, services) {
        this.title = itemInfo.title
        this.desc = itemInfo.desc
        this.newPrice = itemInfo.price
        this.oldPrice = itemInfo.oldPrice
        this.discount = itemInfo.discountDesc
        this.discountBgcolor = itemInfo.discountBgcolor
        this.columns = columns
        this.services = services
        this.realPrice = itemInfo.lowNowPrice
    }
}

export class GoodsParam {
    constructor(info,rule){
        // 注：images可能没有值(某些商品有值，某些没有值)
        this.image = info.images ? info.images[0] : ''
        this.infos = info.set
        this.sizes = rule.tables
    }
}

export class Shop { // 商家信息
    constructor(shopInfo) {
      this.logo = shopInfo.shopLogo;
      this.name = shopInfo.name;
      this.fans = shopInfo.cFans;
      this.sells = shopInfo.cSells;
      this.score = shopInfo.score;
      this.goodsCount = shopInfo.cGoods
    }
  }
  